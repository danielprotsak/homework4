import React, {useEffect, useState} from "react";

export const Photos = ({useHandler}) => {

    let[photos,setPhotos] = useState('');
    let[recievedData,setReceivedData] = useHandler(false);
    useEffect(()=>{
        fetch('https://jsonplaceholder.typicode.com/photos').then(resp=>resp.json()).then(data => {
            setPhotos(data.slice(0,100));
            setReceivedData(true);
        });
    },[recievedData]);
    return (
        <div style={{maxWidth:"300px",border:"1px solid black"}}>
            {photos !== "" &&
            photos.map((item,index) => {
                return (
                    <div key={index}>
                        <span>albumId: {item.albumId}</span>
                        <img width="100" height="100" src={item.url} alt=""/>
                    </div>
                );
            })
            }
        </div>
    );
}