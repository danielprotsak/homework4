import React,{Component} from "react";
import PropTypes from 'prop-types'
import {ThemeContext} from "./Context";
export default class Toggler extends Component {
    state = {toggleKey:"",value:""}
    constructor(props) {
        super(props);
    }
    static getDerivedStateFromProps(props,state){
        const{toggleKey} = state;
        if(toggleKey === ""){
            const{children} = props;
            if(children instanceof Array){
                return {toggleKey:children[0].props.toggleKey,value:children[0].props.value};
            }else{
                return {toggleKey:children.props.toggleKey,value:children[0].props.value}
            }
        }
        return {toggleKey: toggleKey}
    }
    changeToggleValue = (toggleKey,value) => (e) => {
        e.preventDefault();
        this.setState({toggleKey:toggleKey,value:value});
    }
    render = () => {
        const{children,value,label} = this.props;
        const{toggleKey} = this.state;
        return(
            <div className="toggler">
                <h3>{label}</h3>
                <ThemeContext.Consumer>
                    {
                        value => {
                            const{values,style} = value;
                            return(
                                React.Children.map(children, (childrenItem,index) => {
                                    if(React.isValidElement(childrenItem)){
                                        return React.cloneElement(childrenItem,
                                            {...childrenItem,value:values[index], style:{...style}, action:this.changeToggleValue,selected:childrenItem.props.toggleKey === toggleKey});
                                    }
                                })
                            );
                        }
                    }
                </ThemeContext.Consumer>
            </div>
        );
    }

}

export const TogglerItem = ({toggleKey,value,action,selected,style}) => {
    return(
        <button style={style} onClick={action(toggleKey,value)} className={selected?"selected-toggle":"unselected-toggle"}>
            {value}
        </button>
    );
}
Toggler.propTypes = {
    label:PropTypes.string,
    children:PropTypes.array.isRequired
}
TogglerItem.propTypes = {
    key:PropTypes.string,
    value:PropTypes.string,
    action:PropTypes.func
}