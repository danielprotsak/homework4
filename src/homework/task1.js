import React, {Component} from "react";
import Chart from 'chart.js';

export default class Schedule extends Component{
    state = {dataset:[0, 10, 5, 2, 20, 30, 45],chart:null}
    constructor(props) {
        super(props);
        this.canvas = React.createRef();
    }

    static getDerivedStateFromProps (props,state){
        let{chart,dataset} = state;
        if(chart !== null){
            console.log(chart);
            chart.data.datasets[0].data = dataset;
            chart.update();
        }
        return null;
    }
    componentDidMount = () => {
        const ctx = this.canvas.current.getContext('2d');
        const{dataset} = this.state;

         const chart = new Chart(ctx, {
            type: 'line',

            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [{
                    label: 'My First dataset',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: dataset
                }]
            },

            options: {}
        });
         console.log("YES");
         this.setState({chart:chart,dataset});
    }

    randomizeData = (count,min,max) => {
        let arr = [];
        for(let i = 0; i < count; i++){
            arr.push(Math.floor(Math.random() * max)+min)
        }
        return arr;
    }

    changeDataHandler = _ => {
      this.setState({dataset:this.randomizeData(7,0,100)});
    }
    render = () => {
        return(
            <div>
                <button onClick={this.changeDataHandler}>Change data</button>
                <canvas ref={this.canvas}/>
            </div>
        );
    }



}