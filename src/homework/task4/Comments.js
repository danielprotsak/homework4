import React, {useEffect, useState} from "react";

export const Comments = ({useHandler}) => {

    let[comments,setComments] = useState('');
    let[recievedData,setReceivedData] = useHandler(false);
    useEffect(() => {
        fetch(' https://jsonplaceholder.typicode.com/comments').then(resp=>resp.json()).then(data => {
            setComments(data.slice(0,100));
            setReceivedData(true);
        })
    },[recievedData]);

    return(
        <div style={{maxWidth:"300px",border:"1px solid black"}}>
            {comments !== "" &&
            comments.map(item => {
                return (
                    <div key={item.name}>
                        <span>id: {item.id}</span>
                        <p>{item.name}</p>
                    </div>
                );
            })
            }
        </div>
    );
}