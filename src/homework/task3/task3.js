import React, {useContext, useEffect, useRef, useState} from "react";
import {ThemeContext} from "../task2/Context";
import Toggler, {TogglerItem} from "./Toggler";

export const Form = () => {
    const options = ["Kyiv","Odessa","Lviv","Kharkiv"];
    let[userName,setUserName] = useState('');
    let[email,setEmail] = useState('');
    let[dataList,setDataList] = useState([]);
    let[city,setCity] = useState(options[0]);
    let toggleType = useRef(null);

    const userNameHandler = (e) => {
        const value = e.target.value;
        setUserName(value);
    }
    const emailHandler = (e) => {
        const value = e.target.value;
        setEmail(value);
    }
    const cityHandler = (e) => {
        const value = e.target.value;
        setCity(value);
    }
    const onSubmit = (e) => {
        e.preventDefault();
        const obj = {userName,email,city,gender:toggleType.current.state.value};
        console.log(obj);
        setDataList([...dataList,obj]);
        setUserName('');
        setEmail('');
    }
    return(
        <>
        <form onSubmit={onSubmit}>
            <div style={{display:"inline-grid"}}>
                <label htmlFor="username">User name</label>
                <input type="text" name="username" value={userName} onChange={userNameHandler}/>
                <label htmlFor="email">Email</label>
                <input type="text" name="email" value={email} onChange={emailHandler}/>
                <label htmlFor="city">City</label>
                <select value={city} onChange={cityHandler}>
                    {
                        options.map(item => {
                            return(
                                <option key={item}>{item}</option>
                            );
                        })
                    }
                </select>
                <ThemeContext.Provider value={{values:["Men","Female"]}}>
                     <Toggler ref={toggleType} label="Choose your gender">
                       <TogglerItem toggleKey="qwerty" value="Men"/>
                       <TogglerItem toggleKey="asdsa" value="Female"/>
                     </Toggler>

                </ThemeContext.Provider>
                <input type="submit"/>
            </div>
        </form>
            <div>
                {dataList.length > 0 &&
                    <ul>
                        {
                            dataList.map((item,index) => {
                                return(
                                    <li key={index}>{JSON.stringify(item)}</li>
                                );
                            })
                        }
                    </ul>
                }
            </div>
        </>
    );
}