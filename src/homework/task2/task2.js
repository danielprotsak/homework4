import React from "react";
import Toggler,{TogglerItem} from "./Toggler";
import {ThemeContext} from "./Context";



export default class Task2 extends React.Component {
    constructor(props) {
        super(props);
    }
    render = () => {
        return(
            <ThemeContext.Provider value={{values:["Men","Female"],style:{color:"brown"}}}>
                <Toggler label="Task 2">
                    <TogglerItem toggleKey="qwerty"/>
                    <TogglerItem toggleKey="adsadas"/>
                </Toggler>
            </ThemeContext.Provider>
        );
    }

}
