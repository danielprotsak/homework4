import React, {useState} from "react";
import {Comments} from "./Comments";
import {Todos} from "./Todos";
import {Photos} from "./Photos";

const Main = () => {

    const useHandler = () => {
        let[receivedData,isReceived] = useState('');
        const changeStatus = (data) => isReceived(data)
        return[
            receivedData,
            changeStatus
        ]
    }
    return(
        <div className="task4">
            <Comments useHandler={useHandler}/>
            <Todos useHandler={useHandler}/>
            <Photos useHandler={useHandler}/>
        </div>
    );
}
export default Main;