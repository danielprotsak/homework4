import React, {useEffect, useState} from "react";

export const Todos = ({useHandler}) => {
    let[todos,setTodos] = useState('');
    let[recievedData,setReceivedData] = useHandler(false);
    useEffect(()=>{
        fetch('https://jsonplaceholder.typicode.com/todos').then(resp=>resp.json()).then(data=>{
            setTodos(data.slice(0,100));
            setReceivedData(true);
        })
    },[recievedData]);

    return (
        <div style={{maxWidth:"300px",border:"1px solid black"}}>
            {todos !== "" &&
            todos.map(item => {
                return (
                    <div key={item.title}>
                        <span>userId: {item.userId}</span>
                        <p>{item.title}</p>
                    </div>
                );
            })
            }
        </div>
    );
}